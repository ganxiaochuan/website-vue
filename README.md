# 下载地址
#### 前端GIT：
##### 用户界面：https://gitee.com/ganxiaochuan/website-vue
##### 管理员：https://gitee.com/ganxiaochuan/gwdglyht
#### 后端GIT：
##### 后端：https://gitee.com/ganxiaochuan/websitePhp
# 技术栈
##### 前端：VUE，AXIOS，IVIEW，自适应网页
##### 后端：PHP-LARAVEL，MYSQL
# 使用说明
#### 前端：
##### 管理员后台
###### 包含Vuer，iview，axios
###### 1.git clone https://gitee.com/ganxiaochuan/gwdglyht
###### 2.cd gwdglyht
###### 3.npm instal
###### 4.全局搜索$api ，替换为你的后端地址
###### 5.全局搜索http://localhost:8081 ，替换为你的前端用户地址
###### 6.npm run dev
##### 用户界面：
###### 包含Vuer，axios，自适应
###### 1.git clone https://gitee.com/ganxiaochuan/website-vue.git
###### 2.cd website-vue
###### 3.npm install
###### 4.全局搜索$api ，替换为你的后端地址
###### 5.npm start
###### 此项目使用vue cli 生成项目，默认地址http://localhost:8080, 但是建议先开管理员后台为8080端口，这样此项目的端口会被挤到8081端口，当然也可以修改配置，在管理员后端全局搜索http://localhost:8080. 更改为自己的地址
##### 后端
###### 包含php，laravel，mysql
###### 1.git clone https://gitee.com/ganxiaochuan/websitePhp
###### 2.cd websitePhp
###### 3.php artisan serve(需要php环境7.0及以上以及mysql 5.5以上)
# 测试地址
##### 前端：
###### 用户：http://gg.jiushiyouyong.top
###### 管理员：http://admin.jiushiyouyong.top
##### 后端：
###### 后端：http://www.jiushiyouyong.top
###### 用户名：admin
###### 密码：admin
# End